.PHONY: all install

BIN = git-watcher git-home

INSTALL_PATH_BIN ?= /usr/bin/
INST_PATH_BIN = $(abspath $(INSTALL_PATH_BIN))/

all: $(BIN)

install: all | $(INST_PATH_BIN)
	install -m 755 $(BIN) $(INST_PATH_BIN)

%/:
	mkdir -p "$@"
